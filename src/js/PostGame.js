import Phaser from "phaser";
import config from "visual-config-exposer";

class PostGame extends Phaser.Scene {
  constructor() {
    super("endGame");
    this.GAME_WIDTH = 740; //740
    this.GAME_HEIGHT = 900; //900
    this.scores = [];
  }

  init(data) {
    console.log("init", data);
  }

  create(data) {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);

    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on("resize", this.resize, this);

    let bg = this.add.sprite(0, 0, "background");
    bg.setOrigin(0, 0).setDisplaySize(740, 900);

    /*const title = this.add.text(
      this.GAME_WIDTH / 2.5,
      this.GAME_HEIGHT / 10,
      `Highscore : ${localStorage.getItem("Highscore")}`,
      {
        fontFamily: "arial",
        fontSize: "32px",
        fontStyle: "bold",
        fill: "#fff",
      }
    );*/

    const scoreText = this.add.text(
      this.GAME_WIDTH / 3.5,
      this.GAME_HEIGHT / 6,
      `${config.postGameScreen.scoreText}: ${localStorage.getItem("score")}`,
      {
        fontFamily: "arial",
        fontSize: "40px",
        fontStyle: "bold",
        fill: "#fff",
      }
    );

    const playBtn = this.add.text(
      this.GAME_WIDTH / 3,
      this.GAME_HEIGHT / 3 + 50,
      `${config.postGameScreen.againButtonText}`,
      {
        fontSize: "32px",
        fontStyle: "bolder",
        fill: "#fff",
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 3,
      this.GAME_HEIGHT / 2,
      `${config.postGameScreen.submitbuttontext}`,
      {
        fontSize: "32px",
        fontStyle: "bolder",
        fill: "#fff",
      }
    );

    const checksite = this.add.text(
      this.GAME_WIDTH / 3.8,
      this.GAME_HEIGHT / 3 + 250,
      `${config.postGameScreen.checksite}`,
      {
        fontSize: "32px",
        fontStyle: "bolder",
        fill: "#fff",
      }
    );

    let hoverImage = this.add.image(100, 100, "startBall");
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();
    checksite.setInteractive();

    checksite.on("pointerdown", function openExternalLink() {
      const url = config.postGameScreen.ctaUrl;
      const s = window.open(url, "_blank");
      if (s && s.focus) {
        s.focus();
      } else if (!s) {
        window.location.href = url;
      }
    });

    playBtn.on("pointerover", () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width;
      hoverImage.y = playBtn.y + 10;
    });
    playBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    playBtn.on("pointerdown", () => {
      // console.log('redirect to play');
      //this.scene.start("playthis", { score: 0, timer: config.settings.timer});
      window.location.reload(true);
    });

    leaderBoardBtn.on("pointerover", () => {
      hoverImage.setVisible(true);
      hoverImage.x = leaderBoardBtn.x - leaderBoardBtn.width / 2.5;
      hoverImage.y = leaderBoardBtn.y + 10;
    });
    leaderBoardBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    leaderBoardBtn.on("pointerdown", () => {
      // console.log('redirect to play');
      this.scene.start("leaderBoardPost", { score: data.score });
    });

    //this.updateCamera();
    this.scale.on("resize", this.resize, this);
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
    3;
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default PostGame;
